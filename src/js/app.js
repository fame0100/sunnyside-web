document.addEventListener('DOMContentLoaded', function() {
    eventListeners();
});

// Escuchar por un click

function eventListeners() {
    const mobileMenu = document.querySelector('.mobile-menu');
    mobileMenu.addEventListener('click', navegacionResponsive);
    mobileMenu.addEventListener('click', bgMenu);
}

function navegacionResponsive() {
    const navegacion = document.querySelector('.navegacion');
    navegacion.classList.toggle('mostrar'); // toggle - Funciona como un if, si existe el mostrar quitalo y si no agregalo

}

function bgMenu() {
    const bgMenu = document.querySelector('.derecha');
    bgMenu.classList.toggle('bg-menu');
}